# React Quick Start
##### Boilerpla pra começar projetos no React usando Webpack, sass, ES6/7
[Ver sobre plugins de ES6+ do babel](http://babeljs.io/docs/plugins/)

# Starting
```
$ cd react-quick-start
$ npm i
```
# Commands
#### Prepare to production
```
$ npm run prod
```
#### Run Server
```
$ npm run start
```